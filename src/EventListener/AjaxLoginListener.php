<?php


namespace App\EventListener;


use App\Entity\User;
use App\Event\AjaxLoginEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AjaxLoginListener
{
    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function onAjaxLogin(AjaxLoginEvent $event): AjaxLoginEvent
    {
        $responseData = [
            'status' => 'success',
            'userId' => $event->getUser()->getId(),
            'redirectUrl' => $this->getSuccessPageUrl($event->getUser())
        ];

        $event->setResponce(new JsonResponse($responseData));

        return $event;
    }

    private function getSuccessPageUrl(User $user): string
    {
        if(in_array('ROLE_ADMIN', $user->getRoles())) {
            return $this->urlGenerator->generate('user');
        }

        return $this->urlGenerator->generate('only_user');
    }
}