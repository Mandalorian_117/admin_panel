<?php

namespace App\Controller;

use App\Service\FacebookLogin;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils, FacebookLogin $facebookLogin): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $fbLink = $facebookLogin->loginFormFacebook();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
            'fblink'        => $fbLink
        ]);
    }

    /**
     * @Route("/facebook-login-callback", name="fb_callback", schemes={"https"})
     *
     */
    public function facebookLoginCallback(FacebookLogin $facebookLogin)
    {
        $accessToken = $facebookLogin->getAccessToken();
        return $this->render('/mainpage.html.twig', ['token' => $accessToken]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * Render to main page
     * @Route("/", name="mainpage")
     * @return Response
     */
    public function mainpage()
    {
        return $this->render('/mainpage.html.twig');
    }

    /**
     * Render page for user
     * @return Response
     * @Route("/onlyuser", name="only_user")
     */
    public function pageForUser()
    {
        return $this->render('pageforuser.html.twig');
    }
}
