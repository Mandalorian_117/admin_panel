<?php

namespace App\Controller;

use App\Entity\User;
use App\Event\AjaxLoginEvent;
use App\Form\RegistrationFormType;
use App\Form\UserFormType;
use App\Repository\UserRepository;
use App\Security\LoginFormAuthenticator;
use App\Service\FacebookLogin;
use Doctrine\Common\Persistence\ObjectManager;
use Facebook\Facebook;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Component\Security\Guard\PasswordAuthenticatedInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class UserController extends AbstractController
{

    /**
     * Get all registered users
     * @Route("/user", name="user")
     */
    public function index(UserRepository $repository)
    {

        $allUsers = $repository->findAll();
        return $this->render('user/index.html.twig', [
            'users'  => $allUsers,
        ]);
    }

    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Get data from google profile
     * @param UserRepository $repository
     * @param Request $request
     * @return Response
     * @Route("/profile", name="google_profile", options={"expose"=true})
     */
    public function getGoogleProfileData(UserRepository $repository, Request $request, GuardAuthenticatorHandler $guardHandler, EventDispatcherInterface $dispatcher)
    {
        $profileName = $request->get('profile_name');

        if(!empty($profileName)) {
            $user = $repository->findOneBy(['login' => $profileName]);
            $providerKey = 'main';

            if(!empty($user)) {
                $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

                $guardHandler->authenticateWithToken($token, $request, $providerKey);

                return $dispatcher->dispatch(new AjaxLoginEvent($user), AjaxLoginEvent::NAME)
                    ->getResponce();
            } else {
                $newUser = new User();

                $newUser->setLogin($profileName);
                $newUser->setPassword($this->passwordEncoder->encodePassword(
                    $newUser,
                    '123'
                ));
                $newUser->setName('Tester');
                $newUser->setSurname('Soft');
                $newUser->setGender('male');
                $newUser->setDateOfBirth(new \DateTime());
                $newUser->setRoles(array('ROLE_USER'));

                $em = $this->getDoctrine()->getManager();

                $em->persist($newUser);
                $em->flush();

                $request->getSession()->set(\Symfony\Component\Security\Core\Security::LAST_USERNAME, $profileName);
                $token = new UsernamePasswordToken($user, null, $providerKey, $user->getRoles());

                $guardHandler->authenticateWithToken($token, $request, $providerKey);
                return $dispatcher->dispatch(new AjaxLoginEvent($user), AjaxLoginEvent::NAME)
                    ->getResponce();
            }
        }

        return new JsonResponse(['status' => 'fail']);
    }

    /**
     * @param $id
     * @param UserRepository $repository
     * @return Response
     * @Route("/user/single/{id}", name="single_user")
     */
    public function singleview(UserRepository $repository, string $id)
    {
        $user = $repository->findOneBy(['id' => $id]);

        if (!$user){
            throw $this->createNotFoundException();
        }
        return $this->render('user/single.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @param Request $request
     * @return RedirectResponse|Response
     * @Route("/user/create", name="create_user")
     */
    public function createNewUser(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setPassword($passwordEncoder->encodePassword(
                $user,
                $form->get('plainPassword')->getData()
            ));
            $user->setGender($form->get('gender')->getData());
            $user->setRoles([$form->get('roles')->getData()]);
            $user->setDateOfBirth($form->get('dateOfBirth')->getData());

            $em = $this->getDoctrine()->getManager();

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user');
        }
        return $this->render('user/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     * @param User $user
     * @Route("/user/update/{user}", name="update_user")
     * @return RedirectResponse|Response
     */
    public function updateUser(Request $request, User $user)
    {
        $form = $this->createForm(UserFormType::class, $user, [
            'action' => $this->generateUrl('update_user', [
                'user' => $user->getId()
            ]),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $user->setGender($form->get('gender')->getData());

            $em = $this->getDoctrine()->getManager();
            $em->flush();
            return $this->redirectToRoute('user');
        } else {
            echo 'Error';
        }
        return $this->render('user/update.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @param User $user
     * @return RedirectResponse
     * @Route("/user/delete/{user}", name="delete_user")
     */
    public function deleteUser(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('user');
    }
}
