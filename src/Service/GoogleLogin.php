<?php


namespace App\Service;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class GoogleAuthenticationListener implements ListerInterface
{

    private $tokenStorage;

    private $authenticationManager;

    private $providerKey;

    public function handle(RequestEvent $event)
    {
        $request = $event->getRequest();

        $username = '';
        $password = '';

        $authenticatedToken = $this->authenticationManager
            ->authenticate(new UsernamePasswordToken(
                $username,
                $password,
                $this->providerKey));


        $this->tokenStorage->setToken($authenticatedToken);

    }


}