<?php


namespace App\Service;


use Facebook\Exceptions\FacebookSDKException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class FacebookLogin
{
    /**
     * @var UrlGeneratorInterface
     */
    private $urlHelper;

    private $session;

    public function __construct(UrlGeneratorInterface $urlHelper, FacebookSession $session)
    {
        $this->urlHelper = $urlHelper;
        $this->session = $session;
    }

    public function createFacebookInstanse()
    {
        $fb = new \Facebook\Facebook([
            'app_id' => $_SERVER['FACEBOOK_APP_ID'],
            'app_secret' => $_SERVER['FACEBOOK_APP_SECRET'],
            'graph_api_version' => 'v5.0',
            'persistent_data_handler' => $this->session
        ]);
        return $fb;
    }
    /**
     * Get url for log in facebook
     * @return string
     * @throws FacebookSDKException
     */
    public function loginFormFacebook()
    {
        $fb = $this->createFacebookInstanse();

        $helper = $fb->getRedirectLoginHelper();

        $permissions = ['email'];
        $loginUrl = $helper->getLoginUrl(
            $this->urlHelper->generate('fb_callback', [], UrlGeneratorInterface::ABSOLUTE_URL),
            $permissions
        );

        return $loginUrl;
    }

    /**
     * Get access token
     * @return \Facebook\FacebookResponse
     * @throws FacebookSDKException
     */
    public function getAccessToken()
    {
        $fb = $this->createFacebookInstanse();

        $helper = $fb->getRedirectLoginHelper();
        $helper->getPersistentDataHandler()->set('state', $_GET['state']);

        try {
            $accessToken = $helper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: '. $e->getMessage();
            exit;
        } catch (FacebookSDKException $e) {
            echo 'FacebookSDKException: '. $e->getMessage();
        }

        if (!isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }
        // OAuth 2 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('462398664437380');
        // If you know the user ID this access token belongs to, you can validate it here
        $tokenMetadata->validateExpiration();

        if (! $accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
                exit;
            }

        }

        $response = $fb->get('/me?fields=id,name', $accessToken);

        return $response;
    }

}