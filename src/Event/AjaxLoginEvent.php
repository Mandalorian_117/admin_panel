<?php


namespace App\Event;


use App\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Contracts\EventDispatcher\Event;

class AjaxLoginEvent extends Event
{
    public const NAME = 'ajax.login';

    protected $user;
    protected $response;

    public function __construct(User $user)
    {
        $this->user         = $user;
        $this->response     = new JsonResponse();

    }

    public function getUser()
    {
        return $this->user;
    }

    public function setResponce($response)
    {
        $this->response = $response;
    }

    public function getResponce(): Response
    {
        return $this->response;
    }

}